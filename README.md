# EESTEC Server Configuration

This is the main configuration that runs on the EESTEC production server. You can find a more extensive documentation in the internal Google Drive.

## How to deploy from a local machine
To deploy the configuration to production, you have to execute the following steps:
 - Make sure you're running Debian 10 on the server
 - Setup the DNS entries (see documentation on the drive)
 - Add you SSH key to the root user of the server
 - Install Python 3 on the server
 - Install Ansible on your local machine
 - Clone this repository and enter the directory. **Important:** Make sure to recursively clone the repository to include the credentials submodule!
 - Create a file within the root folder of the repository called `.prod_password` and store the ansible-vault production password in it
 - Run `ansible-galaxy install -r requirements.yml` to download all dependencies
 - Run the following command:
   `ansible-playbook site.yml -i inventories/production -i credentials/production --vault-id production@.prod_password`

You can also setup the configuration for the development server by doing the exact same steps as above but replacing 'production' with 'development' and `.prod_password` with `.dev_password`.

After that, you have to restore the backups. To do that, login to the server and run `restore-backup DATE_OF_BACKUP`. Note: You have to put the backup files manually at `/docker/backup/remote` for now.

## [Future] How to deploy using the pipelines
Go to [CI/CD](https://gitlab.com/eestec/eestec-server/-/pipelines) and click ´Run pipeline´. There you can set the branch you want to deploy and set an environment variable called ENV to ´dev´ or ´prod´ depending on which environment you want to deploy to. Optionally, you can provide a TAGS variable, to not run through all roles.