<?php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{ ec_db_name }}');

/** MySQL database username */
define('DB_USER', '{{ ec_db_user }}');

/** MySQL database password */
define('DB_PASSWORD', '{{ ec_db_password }}');

/** MySQL hostname */
define('DB_HOST', 'mysql.docker');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{{ ec_auth_key }}');
define('SECURE_AUTH_KEY',  '{{ ec_secure_auth_key }}');
define('LOGGED_IN_KEY',    '{{ ec_logged_in_key }}');
define('NONCE_KEY',        '{{ ec_nonce_key }}');
define('AUTH_SALT',        '{{ ec_auth_salt }}');
define('SECURE_AUTH_SALT', '{{ ec_secure_auth_salt }}');
define('LOGGED_IN_SALT',   '{{ ec_logged_in_salt }}');
define('NONCE_SALT',       '{{ ec_nonce_salt }}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
