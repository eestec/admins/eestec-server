FROM php:8.2-cli-alpine

# Preinstalled PHP modules:
# Core, ctype, curl, date, dom, fileinfo, filter
# ftp, hash, iconv, json, libxml, mbstring, mysqlnd
# openssl, pcre, PDO, pdo_sqlite, Phar, posix, readline
# Reflection, session, SimpleXML, sodium, SPL, sqlite3
# standard, tokenizer, xml, xmlreader, xmlwriter, zlib

# Utility to install php extensions
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# Install dependencies for the operating system software
# Then clean cache
RUN apk add --no-cache \
    libpq-dev \
    oniguruma \
    libzip-dev \
    postgresql-client

# Install extensions for PHP
RUN  chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions bcmath pdo_pgsql pgsql zip exif pcntl

# user www-data has the uid 82 in alpine, so no need to create it
# Copy the cron file
COPY --chown=www-data:www-data laravel.crontab .

# Giving permission to crontab file
# RUN chmod 0644 /etc/cron.d/laravel.crontab

# Add the laravel cron to the jobs
# It's ran as www-data user (uid 82) so that there are
# no permission issues with the other files in the volume
RUN crontab -u www-data laravel.crontab

# execute crontab with log level 2 on the std out (so we can see it
# on the container logs)
ENTRYPOINT crond -f -l 2 -L /dev/stdout
