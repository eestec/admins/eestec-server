---
- name: Create DNS entries
  block:
    - name: Create DNS A entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ mattermost_service_name }}"
        type: A
        value: "{{ ipv4address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present
    - name: Create DNS AAAA entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ mattermost_service_name }}"
        type: AAAA
        value: "{{ ipv6address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present

## Inside the container the uid and gid is 2000.
- name: Make sure all necessary paths exist
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    mode: u=rwx,g=rx,o=rx
  with_items:
    - { path: /docker/mattermost/config, owner: "2000", group: "2000" }
    - { path: /docker/mattermost/data, owner: "2000", group: "2000" }
    - { path: /docker/mattermost/logs, owner: "2000", group: "2000" }
    - { path: /docker/mattermost/plugins, owner: "2000", group: "2000" }
    - { path: /docker/mattermost/client/plugins, owner: "2000", group: "2000" }
    - { path: /docker/mattermost/bleve-indexes, owner: "2000", group: "2000" }
    - { path: /home/docker/nginx/assets/sites-enabled, owner: root, group: root }

- name: Copy all necessary files to the host
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    mode: u=rw,g=r,o=r
  with_items:
    - { src: nginx.conf, dest: /home/docker/nginx/assets/sites-enabled, dest_name: "{{ mattermost_domain }}" }

- name: Create the necessary things for the database
  block:
    - name: Create the postgres database
      community.postgresql.postgresql_db:
        name: "{{ mattermost_db_name }}"
        state: present
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"
    - name: Create the postgres user
      community.postgresql.postgresql_user:
        name: "{{ mattermost_db_user }}"
        password: "{{ mattermost_db_password }}"
        expires: infinity
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"
    - name: GRANT ALL PRIVILEGES ON DATABASE db TO user
      community.postgresql.postgresql_privs:
        db: postgres
        privs: ALL
        type: database
        obj: "{{ mattermost_db_name }}"
        role: "{{ mattermost_db_user }}"
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"

- name: Ensure latest image is pulled
  community.docker.docker_image_pull:
    name: mattermost/mattermost-team-edition:release-9.8

- name: Build and start the container
  community.docker.docker_container:
    name: mattermost
    image: mattermost/mattermost-team-edition:release-9.8
    state: started
    restart: true
    restart_policy: unless-stopped
    hostname: mattermost.docker
    networks:
      - name: eestec
        aliases:
          - mattermost.docker
    env:
      TZ: CET
      MM_SQLSETTINGS_DRIVERNAME: postgres
      MM_SQLSETTINGS_DATASOURCE: >-
        postgres://{{ mattermost_db_user }}:{{ mattermost_db_password }}@postgres.docker:5432/{{ mattermost_db_name }}?sslmode=disable&connect_timeout=10
      MM_BLEVESETTINGS_INDEXDIR: /mattermost/bleve-indexes
      MM_SERVICESETTINGS_SITEURL: https://{{ mattermost_domain }}
    volumes:
      - /docker/mattermost/config:/mattermost/config:rw
      - /docker/mattermost/data:/mattermost/data:rw
      - /docker/mattermost/logs:/mattermost/logs:rw
      - /docker/mattermost/plugins:/mattermost/plugins:rw
      - /docker/mattermost/client/plugins:/mattermost/client/plugins:rw
      - /docker/mattermost/bleve-indexes:/mattermost/bleve-indexes:rw
