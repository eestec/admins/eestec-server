#!/bin/sh

# Source of script:
# https://stackoverflow.com/questions/57184669/reload-a-letsencrypt-certificate-on-docker

while :; do
    # TODO: We could instead of sleep, detect config changes
    # and only reload if necessary (using inotifywait)
    sleep 24h
    nginx -t && nginx -s reload
done &